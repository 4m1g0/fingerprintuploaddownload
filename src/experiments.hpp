#include "Adafruit-Fingerprint-Sensor-Library/Adafruit_Fingerprint.h"

enum errorCode {
    PACKET_OK = 0,
    INVALID_PACKET_HEADER,
};

void printHex(int num, int precision) {
    char tmp[16];
    char format[128];
 
    sprintf(format, "%%.%dX", precision);
 
    sprintf(tmp, format, num);
    Serial.print(tmp);
}

int downloadFingerprint(Adafruit_Fingerprint finger, int id, SoftwareSerial* serial) {
  Serial.println("------------------------------------");
  Serial.print("Attempting to load #"); Serial.println(id);
  uint8_t p = finger.loadModel(id);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.print("Template "); Serial.print(id); Serial.println(" loaded");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    default:
      Serial.print("Unknown error "); Serial.println(p);
      return p;
  }

  // OK success!

  Serial.print("Attempting to get #"); Serial.println(id);
  p = finger.getModel();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.print("Template "); Serial.print(id); Serial.println(" transferring:");
      break;
   default:
      Serial.print("Unknown error "); Serial.println(p);
      return p;
  }
 // Serial.print("GET STRUCTURED PACKET: ");
  //Serial.print("free stack: "); Serial.println(ESP.getFreeContStack());
  //Serial.print("free heap: "); Serial.println(ESP.getFreeHeap());
  Adafruit_Fingerprint_Packet* packets = new Adafruit_Fingerprint_Packet[12];
  //Serial.print("free stack: "); Serial.println(ESP.getFreeContStack());
  //Serial.print("free heap: "); Serial.println(ESP.getFreeHeap());
  
  uint8_t packetType = 0;
  uint8_t i = 0;
  uint8_t stat = -1;
  while (packetType != FINGERPRINT_ENDDATAPACKET){
    if (i >= 12) {
      Serial.println("Unexpected number of packets...");
      break;
    }
      
    stat = finger.getStructuredPacket(&packets[i++]);
    //Serial.println("HOLA");
    if (stat == FINGERPRINT_BADPACKET) {
      Serial.println("Communication error");
      break;
    }
    packetType = packets[i-1].type;
    //Serial.print("Packet: "); Serial.println(packetType, HEX);
  }
    
  Serial.print("free stack: "); Serial.println(ESP.getFreeContStack());
  Serial.print("free heap: "); Serial.println(ESP.getFreeHeap());
  Serial.print(i); Serial.println(" packets received.");

  //delete [] packets; // when you're done

  if (finger.downloadModel() != FINGERPRINT_OK){
    Serial.println("Errror en downloadModel");
    return 0;
  }
  for (int i = 0; i < 12; i++) {
    Serial.print("Sending packet "); Serial.println(i);
    finger.writeMyStructuredPacket(packets[i]);
    
  }
  /*Adafruit_Fingerprint_Packet packet;
    if (finger.getStructuredPacket(&packet) != FINGERPRINT_OK) {
      Serial.println("Error reading ACK packet");
      return 0;
    }
    if (packet.type != FINGERPRINT_ACKPACKET) {
      Serial.println("Error NACK");
      return 0;
    }*/
  finger.storeModel(5);
  
  if (stat != FINGERPRINT_OK)
    return FINGERPRINT_BADPACKET;

  return FINGERPRINT_OK;
}