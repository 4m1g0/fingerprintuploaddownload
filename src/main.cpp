#include <Arduino.h>
#include "Adafruit-Fingerprint-Sensor-Library/Adafruit_Fingerprint.h"
#include "knownFunctions.hpp"
#include "experiments.hpp"


SoftwareSerial mySerial(D3, D4);

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);


void setup() {
  Serial.begin(115200);
  delay(200); // let fingerprint initialize
  finger.begin(57600);
  if (finger.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    delay(5000);
    ESP.restart();
  }

  Serial.println("Options:");
  Serial.println("1. Enrrol finger on id...");
  Serial.println("2. delete finger with id...");
  Serial.println("3. download finger with id...");
  Serial.println("4. scan finger.");
  Serial.println();
}

void loop() {
  int id;

  while (Serial.available())
  {
    int command = Serial.parseInt();
    switch (command) {
      case 1:
        Serial.print("Id (0 is not valid) to write the fingerprint to: ");
        while(!Serial.available());
        id = Serial.parseInt();
        Serial.println();
        if (id) {
          enroll(finger, id);
        } else {
          Serial.println("Not valid or timeout.");
        }
        break;
      case 2:
        Serial.print("Id of the fingerprint to delete: ");
        while(!Serial.available());
        id = Serial.parseInt();
        Serial.println();
        if (id) {
          deletefinger(finger, id);
        }else {
          Serial.println("Not valid or timeout.");
        }
        break;
      case 3:
        Serial.print("Id of the fingerprint to download: ");
        while(!Serial.available());
        id = Serial.parseInt();
        Serial.println();
        if (id) {
          downloadFingerprint(finger, id, &mySerial);
        }else {
          Serial.println("Not valid or timeout.");
        }
        break;
      case 4:
        Serial.println("Scanning for finger...");
        for (unsigned long t = millis(); t + 10000 > millis();){
          int id = getFingerprintID(finger);
          if (id > 0) {
            Serial.print("Found finger: ");Serial.println(id);
          }
            
        }
    }
  }
}